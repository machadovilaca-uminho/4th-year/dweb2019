const express = require('express');
const router = express.Router();
const Obras = require('../controllers/obras');

router.get('/', (req, res) => {
  Obras.index(req.query)
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

router.get('/:id', (req, res) => {
  Obras.show(req.params.id)
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

module.exports = router;
