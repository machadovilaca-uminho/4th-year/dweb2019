const express = require('express');
const router = express.Router();
const Tipos = require('../controllers/tipos');

router.get('/', (req, res) => {
  Tipos.index()
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

module.exports = router;
