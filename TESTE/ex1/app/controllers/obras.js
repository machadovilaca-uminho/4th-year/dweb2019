const Pub = require('../models/obras');

const Obras = module.exports;

Obras.index = query => {
  switch (true) {
    case query.instrumento !== undefined:
      return Pub
        .find({ 'instrumentos.instrumento': { $elemMatch: { designacao: query.instrumento } } })
        .select({ "_id": 0 })
        .exec();

    case query.compositor !== undefined:
      return Pub
        .find({ compositor: query.compositor })
        .select({ "_id": 0 })
        .exec();

    default:
      return Pub.find().select({ "_id": 0, "-id": 1, titulo: 1, tipo: 1, compositor: 1 }).exec();
  }
};

Obras.show = id => {
  return Pub.findOne({ "-id": id }).select({ "-id": 0 }).exec();
};
