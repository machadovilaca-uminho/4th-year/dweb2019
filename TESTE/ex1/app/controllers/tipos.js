const Obras = require('../models/obras');

const Tipos = module.exports;

Tipos.index = async () => {
  let res = new Set();

  const results = await Obras.find().select('-_id tipo').exec();

  results.forEach(result => {
    result.tipo !== undefined ? res.add(result.tipo) : {} ;
  });

  return new Promise((resolve) => {
    resolve(Array.from(res));
  });
};
