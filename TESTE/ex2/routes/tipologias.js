const express = require('express');
const router = express.Router();

const Tipologias = require('../controllers/tipologias');

router.get('/', (req, res) => {
  return Tipologias.index(req, res);
});

router.get('/:id', (req, res) => {
  return Tipologias.show(req, res, req.params.id);
});

module.exports = router;
