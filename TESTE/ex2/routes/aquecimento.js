const express = require('express');
const router = express.Router();

const Aquecimento = require('../controllers/aquecimento');

router.get('/:id', (req, res) => {
  return Aquecimento.show(req, res, req.params.id);
});

module.exports = router;
1
