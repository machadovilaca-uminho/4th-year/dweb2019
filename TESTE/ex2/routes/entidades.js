const express = require('express');
const router = express.Router();

const Entidades = require('../controllers/entidades');

router.get('/:id', (req, res) => {
  return Entidades.show(req, res, req.params.id);
});

module.exports = router;
