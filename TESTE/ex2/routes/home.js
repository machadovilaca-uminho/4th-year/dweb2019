const express = require('express');
const router = express.Router();

const Entidades = require('../controllers/entidades');

router.get('/', (req, res) => {
  return Entidades.index(req, res);
});

module.exports = router;
