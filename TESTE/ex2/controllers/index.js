const Index = module.exports;

const got = require('got');
const pug = require('pug');

Index.get = async (raw_url) => {
    let url = new URL('http://clav-api.dglab.gov.pt/api/' + raw_url);
    url.searchParams.append('apikey', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Nzg4NjAwNTQsImV4cCI6MTU4MTQ1MjA1NH0.HIlH4_Ao6504qaLhhbZ2_OtDzaZaG5FeYy-Yc2d9lwQ');

    const response = await got(url);
    return JSON.parse(response.body);
};

Index.success = (res, path, options) => {
    res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
    res.write(pug.renderFile(path, options));
    res.end();
};

Index.error = (err, res) => {
    console.log(err);

    res.writeHead(400, { 'Content-Type': 'text/html; charset=utf-8' });
    res.write(pug.renderFile('view/error.pug', { status: 400 }));
    res.end();
};
