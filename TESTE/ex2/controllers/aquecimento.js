const Aquecimento = module.exports;

const Index = require('./index');

Aquecimento.show = (req, res, id) => {

  switch (id) {
    case "1":
      Index.get('entidades').then(r => {
        res.jsonp({ "Quantas Entidades estão catalogadas?": r.length });
        res.end();
      }).catch((err) =>
        Index.error(err, res)
      );
      break;

    case "2":
      Index.get('entidades/ent_ACT/tipologias').then(r => {
        res.jsonp({ "A que Tipologias pertence a Entidade \"Autoridade para as Condições de Trabalho\"": r });
        res.end();
      }).catch((err) =>
        Index.error(err, res)
      );
      break;

    case "3":
      Index.get('entidades/ent_ANSR/intervencao/participante').then(r => {
        res.jsonp({ "Em que processos a entidade \"Autoridade Nacional de Segurança Rodoviária\" participa como Iniciador?": r });
        res.end();
      }).catch((err) =>
        Index.error(err, res)
      );
      break;

    case "4":
      Index.get('entidades/ent_CMil/intervencao/dono').then(r => {
        res.jsonp({
          "De que processos é dona a entidade \"Colégio Militar\"?": r.map(p => p.titulo)
        });
        res.end();
      }).catch((err) =>
        Index.error(err, res)
      );
      break;

    default:
      Index.error("Not question matched", res)
  }
};
