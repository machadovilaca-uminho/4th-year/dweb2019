const Entidades = module.exports;

const Index = require('./index');

Entidades.index = (req, res) => {
  Index.get('entidades').then(r => {
    let n = 0;
    let p = 0;
    let entidades = [];

    if(req.query.page !== undefined) {
      n = req.query.page * 10;
      p = parseInt(req.query.page);
    }

    for (let x = n; x < n + 10; x++) {
      entidades.push(r[x]);
    }

    const previous = p === 0 ? 0 : p-1;
    const next = p === 54 ? 54 : p+1;

    Index.success(res, 'view/elementos.pug', { entidades: entidades, page: p, next: next, previous: previous })
  }).catch((err) =>
    Index.error(err, res)
  );
};

Entidades.show = (req, res, id) => {
  Index.get(`entidades/${id}`).then(r =>
    Index.get(`entidades/${id}/tipologias`).then(e =>
      Index.get(`entidades/${id}/intervencao/dono`).then(d =>
        Index.get(`entidades/${id}/intervencao/participante`).then(p =>
          Index.success(res, 'view/elemento.pug', {
            entidade: r,
            tipologias: e ,
            dona: d,
            participa: p
          })
        )
      )
    )
  ).catch((err) =>
    Index.error(err, res)
  );
};
