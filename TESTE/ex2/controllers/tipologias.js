const Tipologias = module.exports;

const Index = require('./index');

Tipologias.index = (req, res) => {
  Index.get('tipologias').then(r => {
    let n = 0;
    let tipologias = [];

    if(req.query.page !== undefined) {
      n = req.query.page * 10;
    }

    for (let x = n; x < n + 10; x++) {
      tipologias.push(r[x]);
    }

    Index.success(res, 'view/tipologias.pug', { tipologias: tipologias })
  }).catch((err) =>
    Index.error(err, res)
  );
};

Tipologias.show = (req, res, id) => {
  Index.get(`tipologias/${id}`).then(r =>
    Index.get(`tipologias/${id}/elementos`).then(e =>
      Index.success(res, 'view/tipologia.pug', { tipologia: r, elementos: e })
    )
  ).catch((err) =>
    Index.error(err, res)
  );
};
