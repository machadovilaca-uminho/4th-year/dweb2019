const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const pug = require('pug');

const homeRouter = require('./routes/home');
const aquecimentoRouter = require('./routes/aquecimento');
const entidadesRouter = require('./routes/entidades');
const tipologiasRouter = require('./routes/tipologias');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use('/', homeRouter);
app.use('/aquecimento', aquecimentoRouter);
app.use('/entidades', entidadesRouter);
app.use('/tipologias', tipologiasRouter);

app.use((req, res, next) => {
  next(error(res, 404));
});

error = (res, status) => {
  res.writeHead(status, { 'Content-Type': 'text/html; charset=utf-8' });
  res.write(pug.renderFile('view/error.pug', { status: status }));
  res.end();
};

module.exports = app;
