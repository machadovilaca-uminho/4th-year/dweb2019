<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8"/>

    <xsl:template match="pr">
        <html>
            <head>
                <title><xsl:value-of select="./metadata/title"/></title>
            </head>

            <body>
                <h1><xsl:value-of select="./metadata/title"/></h1>
                <h2><xsl:value-of select="./metadata/subtitle"/></h2>
                <h4><xsl:value-of select="./metadata/keyname"/></h4>

                <br/>

                <p><strong>Supervisor: </strong>
                    <a>
                        <xsl:attribute name="href"><xsl:value-of select="./metadata/supervisor/@homepage"/></xsl:attribute>
                        <xsl:value-of select="./metadata/supervisor"/>
                    </a>
                </p>
                <p><strong>Data:</strong> <xsl:value-of select="./metadata/bdate"/> - <xsl:value-of select="./metadata/edate"/></p>

                <hr/>

                <h4>Team</h4>
                <ul>
                    <xsl:for-each select="./workteam/worker">
                        <li>
                            <xsl:value-of select="./identifier"/> - <xsl:value-of select="./name"/> (
                            <a>
                                <xsl:attribute name="href">mailto:<xsl:value-of select="./email"/></xsl:attribute>
                                <xsl:value-of select="./email"/>
                            </a>,
                            <a>
                                <xsl:attribute name="href"><xsl:value-of select="./git"/></xsl:attribute>
                                <xsl:attribute name="target">_blank</xsl:attribute>
                                <xsl:value-of select="./git"/>
                            </a>)
                        </li>
                    </xsl:for-each>
                </ul>

                <hr/>

                <h4>Abstract</h4>
                <xsl:for-each select="./abstract/p">
                    <p><xsl:value-of select="."/></p>
                </xsl:for-each>

                <hr/>

                <h4>Deliverables</h4>
                <ul>
                    <xsl:for-each select="./deliverables/deliverable">
                        <li><a>
                            <xsl:attribute name="href"><xsl:value-of select="./@path"/></xsl:attribute>
                            <xsl:value-of select="."/>
                        </a></li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>