<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:output method="xhtml" indent="yes" encoding="UTF-8"/>

    <xsl:template match="/">
        <xsl:result-document href="html/index.html">
            <html>
                <head>
                    <title>Arqueossítios</title>
                </head>
                <body>
                    <h1>Arqueossítios</h1>
                    <ul>
                        <xsl:apply-templates mode="indice"/>
                    </ul>
                </body>
            </html>
        </xsl:result-document>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="ARQELEM" mode="indice">
        <li>
            <a name="{generate-id()}"/>
            <a href="arq-{generate-id()}.html"><xsl:value-of select="IDENTI"/></a>
        </li>
    </xsl:template>

    <xsl:template match="ARQELEM">
        <xsl:result-document href="html/arq-{generate-id()}.html">
            <html>
                <head>
                    <title>ARQSITS</title>
                </head>

                <body>
                    <h2><xsl:value-of select="./IDENTI"/></h2>
                    (<xsl:value-of select="./TIPO/@ASSUNTO"/>)

                    <br/>

                    <xsl:if test="boolean(./IMAGEM)">
                        <img>
                            <xsl:attribute name="src"><xsl:value-of select="./IMAGEM/@NOME"/></xsl:attribute>
                        </img>
                    </xsl:if>

                    <br/>

                    <xsl:if test="boolean(./CRONO)">
                        <strong>Período: </strong><xsl:value-of select="./CRONO"/>
                        <br/>
                    </xsl:if>

                    <strong>Local: </strong><xsl:value-of select="./CONCEL"/>, <xsl:value-of select="./FREGUE"/>, <xsl:value-of select="./LUGAR"/>
                    <xsl:if test="boolean(./CODADM)">
                        <span>, <xsl:value-of select="./CODADM"/></span>
                    </xsl:if>

                    <br/>

                    <xsl:if test="boolean(./LATITU)">
                        <span><strong>Latitude: </strong><xsl:value-of select="./LATITU"/></span>
                    </xsl:if>
                    <xsl:if test="boolean(./LONGIT)">
                        <span>, <strong>Longitude: </strong><xsl:value-of select="./LONGIT"/></span>
                    </xsl:if>
                    <xsl:if test="boolean(./ALTITU)">
                        <span>, <strong>Altitude: </strong><xsl:value-of select="./ALTITU"/></span>
                    </xsl:if>

                    <br/><br/>

                    <span><strong>Descrição: </strong><xsl:value-of select="./DESCRI"/></span>

                    <br/>

                    <xsl:if test="boolean(./ACESSO)">
                        <span><strong>Acessos: </strong><xsl:value-of select="./ACESSO"/></span>
                        <br/><br/>
                    </xsl:if>

                    <xsl:if test="boolean(./QUADRO)">
                        <xsl:value-of select="./QUADRO"/>
                        <br/>
                    </xsl:if>

                    <br/>

                    <xsl:for-each select="./DESARQ">
                        <span><strong>DESARQ: </strong><xsl:value-of select="."/></span>
                        <br/>
                    </xsl:for-each>

                    <br/>

                    <xsl:for-each select="./INTERP">
                        <span><strong>INTERP: </strong><xsl:value-of select="."/></span>
                        <br/>
                    </xsl:for-each>

                    <br/>

                    <xsl:for-each select="./DEPOSI">
                        <span><strong>DEPOSI: </strong><xsl:value-of select="."/></span>
                        <br/>
                    </xsl:for-each>

                    <br/>

                    <xsl:for-each select="./BIBLIO">
                        <span><strong>BIBLIO: </strong><xsl:value-of select="."/></span>
                        <br/>
                    </xsl:for-each>

                    <br/>

                    <xsl:for-each select="./AUTOR">
                        <span><strong>AUTOR: </strong><xsl:value-of select="."/></span>
                        <br/>
                    </xsl:for-each>

                    <br/>

                    <xsl:for-each select="./TRAARQ">
                        <span><strong>TRAARQ: </strong><xsl:value-of select="."/></span>
                        <br/>
                    </xsl:for-each>

                    <br/>

                    <xsl:for-each select="./INTERE">
                        <span><strong>INTERE: </strong><xsl:value-of select="."/></span>
                        <br/>
                    </xsl:for-each>

                    <br/>

                    <span><strong>Data: </strong><xsl:value-of select="./DATA"/></span>

                    <br/><br/>

                    <hr/>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

</xsl:stylesheet>