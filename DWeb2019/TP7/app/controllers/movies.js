const Movie = require('../models/movie');

const Movies = module.exports;

Movies.index = () => {
  return Movie.find().sort({ title: 'asc' }).exec();
};

Movies.show = id => {
  return Movie.findById(id).exec();
};

Movies.store = data => {
  return Movie.create(data);
};

Movies.update = (id, data) => {
  return Movie.findByIdAndUpdate(id, data).exec();
};

Movies.destroy = id => {
  return Movie.findByIdAndDelete(id).exec();
};
