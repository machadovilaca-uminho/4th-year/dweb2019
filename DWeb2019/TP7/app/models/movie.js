const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MovieSchema = new Schema({
  cast: Array,
  genres: Array,
  title: {
    required: true,
    type: String
  },
  year: {
    required: true,
    type: Number
  }
});

module.exports = mongoose.model('movies', MovieSchema);
