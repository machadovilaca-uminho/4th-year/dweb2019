const express = require('express');
const router = express.Router();
const Movies = require('../controllers/movies');

router.get('/', (req, res) => {
  Movies.index()
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

router.get('/:id', (req, res) => {
  Movies.show(req.params.id)
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

router.post('/', (req, res) => {
  Movies.store(req.body)
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

router.put('/:id', (req, res) => {
  Movies.update(req.params.id, req.body)
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

router.delete('/:id', (req, res) => {
  Movies.destroy(req.params.id)
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

module.exports = router;
