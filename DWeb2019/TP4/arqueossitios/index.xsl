<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
    <xsl:template match="/">
        <xsl:result-document href="index.html">
            <html>
                <head><title>Arqueossítios</title></head>
                <body>
                    <h1>Arqueossítios</h1><ul>
                    <xsl:apply-templates mode="indice"/>
                </ul></body>
            </html>
        </xsl:result-document>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="ARQELEM" mode="indice">
        <li>
            <a name="{count(preceding-sibling::*)+1}"/>
            <a href="{count(preceding-sibling::*)+1}"><xsl:value-of select="IDENTI"/></a>
        </li>
    </xsl:template>
</xsl:stylesheet>
