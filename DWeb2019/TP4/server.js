const http = require('http');
const url = require('url');
const fs = require('fs');
const logger = require('debugnyan')('server');

function serveFile(path, type, res) {
  fs.readFile(path, (err, data) => {
    if (err) {
      res.writeHead(400, { ContentType: 'text/plain' });
      res.write(`${err.syscall} on ${err.path} failed`);
      logger.debug(`${err.syscall} on ${err.path} failed`);
    } else {
      res.writeHead(200, { ContentType: type });
      res.write(data);
    }

    res.end();
  });
}

function handle(req, res) {
  const filename = url.parse(req.url).pathname.split('/')[1];

  if (filename === '') {
    serveFile(`arqueossitios/index.html`, 'text/html', res);
  } else if (filename === 'arq2html.xsl') {
    serveFile(`arqueossitios/arq2html.xsl`, 'text/xsl', res);
  } else {
    serveFile(`arqueossitios/dataset/arq${filename}.xml`, 'text/xml', res);
  }
}

http
  .createServer((req, res) => {
    logger.debug(`${req.method} ${req.url}`);
    handle(req, res);
  })
  .listen(7777);
