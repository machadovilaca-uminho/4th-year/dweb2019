const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Laureate = require('./laureate').schema;

const PrizeSchema = new Schema({
  category: String,
  laureates: [Laureate],
  overallMotivation: String,
  year: String
});

module.exports = mongoose.model('prizes', PrizeSchema);
