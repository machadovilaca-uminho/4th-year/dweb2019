const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LaureateSchema = new Schema({
  firstname: String,
  id: String,
  motivation: String,
  share: String,
  surname: Number
});

module.exports = mongoose.model('laureates', LaureateSchema);
