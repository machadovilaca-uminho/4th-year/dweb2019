const express = require('express');
const router = express.Router();
const Categories = require('../controllers/categories');

router.get('/', (req, res) => {
  Categories.index()
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

module.exports = router;
