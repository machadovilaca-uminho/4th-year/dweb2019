const express = require('express');
const router = express.Router();
const Laureates = require('../controllers/laureates');

router.get('/', (req, res) => {
  Laureates.index()
    .then(data => res.jsonp(data[0].images))
    .catch(err => res.status(400).jsonp(err));
});

module.exports = router;
