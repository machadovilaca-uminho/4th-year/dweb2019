const express = require('express');
const router = express.Router();
const Prizes = require('../controllers/prizes');

router.get('/', (req, res) => {
  Prizes.index(req.query)
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

router.get('/:id', (req, res) => {
  Prizes.show(req.params.id)
    .then(data => res.jsonp(data))
    .catch(err => res.status(400).jsonp(err));
});

module.exports = router;
