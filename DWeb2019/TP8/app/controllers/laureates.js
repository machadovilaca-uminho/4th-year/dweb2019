const Prize = require('../models/prize');

const Laureates = module.exports;

Laureates.index = () => {
  return Prize.aggregate([
    {
      $group: {
        _id: null,
        laureates: { $push: '$laureates' }
      }
    },
    {
      $project: {
        _id: 0,
        images: {
          $reduce: {
            in: {
              $concatArrays: ['$$this', '$$value']
            },
            initialValue: [],
            input: '$laureates'
          }
        }
      }
    }
  ]).exec();
};
