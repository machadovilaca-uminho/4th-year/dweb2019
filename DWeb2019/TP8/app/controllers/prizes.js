const Prize = require('../models/prize');

const Prizes = module.exports;

Prizes.index = queryStrings => {
  const filter = {};

  if (queryStrings.categoria !== undefined) {
    filter.category = queryStrings.categoria;
  }

  if (queryStrings.data !== undefined) {
    filter.year = { $gt: queryStrings.data };
  }

  return Prize.find(filter, 'year category').sort({ year: 'desc' }).exec();
};

Prizes.show = id => {
  return Prize.findById(id).exec();
};
