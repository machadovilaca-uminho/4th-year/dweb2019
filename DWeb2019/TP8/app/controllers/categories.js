const Prize = require('../models/prize');

const Categories = module.exports;

Categories.index = () => {
  return Prize.distinct('category').exec();
};
