const logger = require('debugnyan')('server');
const express = require('express');
const arqRoutes = require('./arq');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', path.join(__dirname, '/view'));
app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, '/public')));

app.all('/*', (req, res, next) => {
  logger.debug(`${req.method} ${req.url}`);
  next();
});

arqRoutes(app, logger);

app.listen(7777, () => logger.debug(`Server listening on port 7777!`));
