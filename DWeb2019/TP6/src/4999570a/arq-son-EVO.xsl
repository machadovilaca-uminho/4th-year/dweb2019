<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.1">

    <xsl:output method="text" encoding="UTF-8"/>

    <xsl:template match="/">
        <xsl:text>{ "nextId": "</xsl:text>
        <xsl:value-of select="count(/arq/doc)"/>
        <xsl:text>", "musics": [</xsl:text>
        <xsl:apply-templates mode="arq"/>
        <xsl:text>]}</xsl:text>
    </xsl:template>

    <xsl:template match="doc" mode="arq">
        {
            "id": "<xsl:value-of select="count(preceding-sibling::*)"/>",

            "prov": "<xsl:value-of select="normalize-space(./prov)"/>",

            "local": "<xsl:value-of select="normalize-space(./local)"/>",

            "tit": "<xsl:value-of select="normalize-space(./tit)"/>",

            "musico": "<xsl:value-of select="normalize-space(./musico)"/>",

            <xsl:if test="./obs">"obs": "<xsl:value-of select="normalize-space(./obs)"/>",</xsl:if>

            <xsl:if test="./inst">"inst": "<xsl:value-of select="normalize-space(./inst)"/>",</xsl:if>

            "file": {
                "name": "<xsl:value-of select="normalize-space(./file)"/>",
                "t": "<xsl:value-of select="normalize-space(./file/@t)"/>"
            },

            "duracao": "<xsl:value-of select="normalize-space(./duracao)"/>"
        }
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
</xsl:stylesheet>
