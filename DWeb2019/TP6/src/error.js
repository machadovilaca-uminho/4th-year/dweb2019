module.exports = function error(err, res, logger) {
  logger.debug(err);
  res.status(400);
  res.render('error');
};
