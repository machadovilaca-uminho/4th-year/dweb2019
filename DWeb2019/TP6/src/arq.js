const error = require('./error');
const jsonfile = require('jsonfile');

function writeNewDB(data, res) {
  jsonfile.writeFile('src/4999570a/db.json', data, err => {
    if (err) {
      error(err, res);
    }
  });
}

module.exports = function arqRoutes(app, logger) {
  app.get('/arq', (req, res) => {
    jsonfile.readFile('src/4999570a/db.json', (err, data) => {
      if (!err) {
        res.status(200);
        res.render('index', { data: data.musics });
      } else {
        error(err, res, logger);
      }
    });
  });

  app.get('/arq/create', (req, res) => {
    res.status(200);
    res.render('add');
  });

  app.post('/arq', (req, res) => {
    const qs = req.body;

    jsonfile.readFile('src/4999570a/db.json', (err, data) => {
      if (!err) {
        qs.id = data.nextId;
        data.musics.push(qs);
        data.nextId++;

        writeNewDB(data, res);
        res.redirect(`/arq`);
      } else {
        error(err, res, logger);
      }
    });
  });

  app.get('/arq/:id', (req, res) => {
    jsonfile.readFile('src/4999570a/db.json', (err, data) => {
      if (!err) {
        for (let i = 0; i < data.musics.length; i++) {
          if (data.musics[i].id === req.params.id) {
            res.status(200);
            res.render('show', { music: data.musics[i] });
            res.end();
          }
        }
      } else {
        error(err, res, logger);
      }
    });
  });

  app.get('/arq/:id/delete', (req, res) => {
    jsonfile.readFile('src/4999570a/db.json', (err, data) => {
      if (!err) {
        for (let i = 0; i < data.musics.length; i++) {
          if (data.musics[i].id === req.params.id) {
            data.musics.splice(i, 1);
            break;
          }
        }
        writeNewDB(data, res);
        res.redirect('/arq');
      } else {
        error(err, res, logger);
      }
    });
  });
};
