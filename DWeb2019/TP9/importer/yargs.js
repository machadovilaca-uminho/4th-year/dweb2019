module.exports = function(yargs) {
  return yargs
    .usage('Usage: $0 [options]')
    .example('$0  -h localhost -p 27017 -d database -c collection dataset.json --jsonArray dataset.json')
    .alias('h', 'host') .nargs('h', 1).describe('h', 'host')
    .alias('p', 'port') .nargs('p', 1).describe('p', 'port')
    .alias('d', 'db') .nargs('d', 1).describe('d', 'database')
    .alias('c', 'collection').nargs('d', 1).describe('d', 'collection')
    .alias('j', 'jsonArray').nargs('j', 1).describe('j', 'file')
    .demandOption(['d', 'c', 'j'])
    .help('help')
    .argv;
};
