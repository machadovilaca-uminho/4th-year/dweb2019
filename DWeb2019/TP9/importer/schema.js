const mongoose = require('mongoose');

module.exports = function(collection) {
  const anySchema = new mongoose.Schema({ _id: String, any: mongoose.Schema.Types.Mixed }, { strict: false });

  return mongoose.model(collection, anySchema);
};
