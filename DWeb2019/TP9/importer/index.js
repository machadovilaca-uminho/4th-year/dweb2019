const connectMongo = require('./db');
const yargs = require('yargs');
const setupYargs = require('./yargs');
const setupSchema = require('./schema');
const fs = require('fs');
const through = require('through2');
const jsonArrayStreams = require('json-array-streams');

const argv = setupYargs(yargs);

if (argv.host === undefined) {
  argv.host = 'localhost';
}

if (argv.port === undefined) {
  argv.port = '27017';
}

async function connectDatabase() {
  await connectMongo(argv.host, argv.port, argv.database);

  return setupSchema(argv.collection);
}

async function parseData(model) {
  const load = [];

  const stream = fs.createReadStream(argv.jsonArray)
    .pipe(jsonArrayStreams.parse())
    .pipe(through.obj((object, enc, cb) => {
      load.push(object);
      cb();
    }));

  return await new Promise(resolve => {
    stream.on('finish', async () => {
      await model.insertMany(load);
      resolve();
    });
  });
}

async function importDataset() {
  const model = await connectDatabase();

  await parseData(model);
}

importDataset()
  .then(() => {
    console.log(`Done`);
    process.exit(0);
  }).catch(err => {
    console.log(`Import failed ${err}`);
    process.exit(1);
  });
