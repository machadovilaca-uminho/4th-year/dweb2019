const mongoose = require('mongoose');

module.exports = async function(host, port, name) {
  return await new Promise((resolve, reject) => {
    mongoose.connect(`mongodb://${host}:${port}/${name}`, { useNewUrlParser: true, useUnifiedTopology: true })
      .then(() => resolve())
      .catch(err => reject(err));
  });
};
