const http = require('http');
const pug = require('pug');
const jsonfile = require('jsonfile');
const { parse } = require('querystring');
const logger = require('debugnyan')('server');

function error(err, res) {
  logger.debug(err);
  res.writeHead(400, { 'Content-Type': 'text/html; charset=utf-8' });
  res.write(pug.renderFile('view/error.pug'));
  res.end();
}

function handleGet(res) {
  jsonfile.readFile('db/db.json', (err, data) => {
    if (!err) {
      res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
      res.write(pug.renderFile('view/index.pug', { tasks: data.tasks }));
      res.end();
    } else {
      error(err, res);
    }
  });
}

function writeNewDB(data, res) {
  jsonfile.writeFile('db/db.json', data, err => {
    if (!err) {
      res.writeHead(302, { Location: '/' });
      res.end();
    } else {
      error(err, res);
    }
  });
}

function handlePost(req, res) {
  let body = '';

  req.on('data', chunk => {
    body += chunk.toString();
  });

  req.on('end', () => {
    const qs = parse(body);

    jsonfile.readFile('db/db.json', (err, data) => {
      if (!err) {
        qs.id = data.nextId;
        data.tasks.push(qs);
        data.nextId++;

        writeNewDB(data, res);
      } else {
        error(err, res);
      }
    });
  });
}

function handleDelete(req, res) {
  let body = '';

  req.on('data', chunk => {
    body += chunk.toString();
  });

  req.on('end', () => {
    const id = parseInt(parse(body).id, 10);

    jsonfile.readFile('db/db.json', (err, data) => {
      if (!err) {
        for (let i = 0; i < data.tasks.length; i++) {
          if (data.tasks[i].id === id) {
            data.tasks.splice(i, 1);
            break;
          }
        }
        writeNewDB(data, res);
      } else {
        error(err, res);
      }
    });
  });
}

function handle(req, res) {
  if (req.method === 'POST' && req.url.startsWith('/delete')) {
    handleDelete(req, res);
  } else if (req.method === 'GET') {
    handleGet(res);
  } else if (req.method === 'POST') {
    handlePost(req, res);
  } else {
    logger.debug(`${req.method} can't be handled`);
  }
}

http
  .createServer((req, res) => {
    logger.debug(`${req.method} ${req.url}`);
    handle(req, res);
  })
  .listen(7777);
